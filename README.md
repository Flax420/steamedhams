# SteamedHams

This is a project made for fun to host a simple file sharing website inspired by [0x0](https://github.com/lachs0r/0x0). 

# Requirements
* Python3  
* flask
* flask_sqlalchemy

# How to run

Simply run app.py with Python 3 to start the development server, though this is not recommended for actual use  
```  
python3 app.py  
```  

# TODO  
Later down the line I want to add the cleanup script and generally clean up the code a bit.  
