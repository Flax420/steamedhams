from flask import Flask, render_template, request, send_from_directory
from flask_sqlalchemy import SQLAlchemy
import time
import hashlib
import datetime
import base64
import mimetypes

# Initializing app
app = Flask(__name__)
app.config["EXTERNAL_URL"] = "http://example.com/" # External hostname, this can be fetched automagically but I don't really know how
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///filestore.db"
app.config["APP_STORAGE_PATH"] = "files"
app.config["PREFERRED_URL_SCHEME"] = "https"
app.config["MAX_CONTENT_LENGTH"] = 1024 * 1024 * 1024 # 1 GB
app.config["EXPIRATION_TIME"] = 3600 * 24 * 30 # Files are stored for 1 month
db = SQLAlchemy(app)

class FileContents(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300))
    filename = db.Column(db.String(300))
    urlname = db.Column(db.String(300))
    expiration_date = db.Column(db.Integer)

    def __init__(self, name, filename, urlname, expiration_date):
        self.name = name
        self.filename = filename
        self.urlname = urlname
        self.expiration_date = expiration_date


@app.route("/", methods=["GET","POST"])
def index():
    if request.method == "GET":
        return render_template("index.html")
    else:
        now = datetime.datetime.now()
        file = request.files["file"]
        fileEnding = file.filename.split(".")[-1]

        total = len(FileContents.query.all())


        newname = file.filename
        newfilename = hashlib.sha224(str.encode(str(int(time.time())))).hexdigest()# Filename for saved file
        new_expiration_date = time.time() + app.config["EXPIRATION_TIME"] # Add expiration date
        newurlname = str(base64.urlsafe_b64encode(str.encode(str(int(total)))))[:-1] + "." + fileEnding # Making a url name, current timestamp in base64 + line ending
        newurlname = newurlname[2:]
        # Commiting to database
        newFile = FileContents(name=newname, filename=newfilename, urlname=newurlname, expiration_date=int(new_expiration_date))
        db.session.add(newFile)
        db.session.commit()

        #Saving file to disk
        rawfile = file.read()
        filename = app.config["APP_STORAGE_PATH"] + "/" + newfilename
        with open(filename, "wb") as f:
            f.write(rawfile)

        return app.config["EXTERNAL_URL"] + newurlname + "\n"


@app.route("/<path:path>")
def get_file(path):
    try: 
        # Creating a query and serving file to user
        query = FileContents.query.filter_by(urlname=str(path)).first()
        fileEnd = path.split(".")[-1]
        return send_from_directory(app.config["APP_STORAGE_PATH"], query.filename, mimetype=mimetypes.types_map["." + fileEnd])
    except:
        # File could not be found, displays 404 instead
        return "404: File not found"


if __name__ == "__main__":
    app.run(port=80, host="0.0.0.0")